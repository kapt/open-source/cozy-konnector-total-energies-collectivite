const {
  BaseKonnector,
  requestFactory,
  log,
  errors
} = require('cozy-konnector-libs')
const slugify = require('cozy-slug')
const { parse } = require('date-fns')
const stream = require('stream')

// Global constants
const VENDOR = 'Total energies collectivité'

// The URLs of the client area
const BASE_URL = 'https://pro.totalenergies.fr/'
const EXTRA_VARS_URL = `${BASE_URL}extra-vars.js`
let BACKEND_URL = undefined

let authenticateToken = undefined // A global variable to store the auth Token retrieved by the authenticate request

// Requests constants
const cookieJar = requestFactory().jar()
const userAgent =
  'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'

// Initialize different kinds of requests

const requestParams = {
  // The debug mode shows all the details about HTTP requests and responses. Very useful for
  // debugging but very verbose. This is why it is commented out by default
  debug: false,

  // If cheerio is activated do not forget to deactivate json parsing (which is activated by
  // default in cozy-konnector-libs
  json: false,

  // This allows request-promise to keep cookies between requests
  jar: cookieJar,

  headers: {
    'User-Agent': userAgent,
    'Accept-Language': 'fr-FR,fr;q=0.9'
  },

  // Inject full response on requests, allowing to check for response status code
  resolveWithFullResponse: true
}

const docRequestAccept =
  'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8'

const XHRAccept = 'application/json, text/javascript, */*; q=0.01'

// A [request](https://github.com/request/request-promise) instance, with cheerio and for document requests
let cheerioDocRequest

// A [request](https://github.com/request/request-promise) instance, without cheerio and for document requests
let rawDocRequest

// A [request](https://github.com/request/request-promise) instance, without cheerio, with .
let rawXHRRequest

function callRequestFactories() {
  if (authenticateToken) {
    requestParams.headers['Authorization'] = `Bearer ${authenticateToken}`
  }

  // A [request](https://github.com/request/request-promise) instance, with cheerio and for document requests
  cheerioDocRequest = requestFactory({
    ...requestParams,

    headers: { ...requestParams.headers, Accept: docRequestAccept },

    // Activates [cheerio](https://cheerio.js.org/) parsing on each page
    cheerio: true
  })

  // A [request](https://github.com/request/request-promise) instance, without cheerio and for document requests
  rawDocRequest = requestFactory({
    ...requestParams,

    headers: { ...requestParams.headers, Accept: docRequestAccept },

    // Des-activate [cheerio](https://cheerio.js.org/) parsing
    cheerio: false
  })

  // A [request](https://github.com/request/request-promise) instance, without cheerio, with .
  rawXHRRequest = requestFactory({
    ...requestParams,

    json: true,

    headers: {
      ...requestParams.headers,
      Accept: XHRAccept,
      'Content-Type': 'application/json',
      'accept-api-version': 'protocol=1.0,resource=2.0'
    },

    // Des-activate [cheerio](https://cheerio.js.org/) parsing
    cheerio: false
  })
}

/*
A wrapper around request-promise requests to perform a GET request, checking the status code of the request.

Args:
  - uri: The URL of the request to perform
  - qs: The query params
  - type: The type of request to perform. Can be "Doc" or "XHR"
  - cheerio: Indicates if cheerio should be used to parse the response

Returns:
- The full request
*/
async function GETRequest(uri, qs = {}, type = 'Doc', cheerio = true) {
  // log('debug', `GET request to : ${uri}`)
  // log('debug', 'with query : ')
  // log('debug', qs)
  // log('debug', 'of type : ' + type)
  // log('debug', 'with cheerio : ' + cheerio)

  let fullResponse

  if (type === 'Doc') {
    if (cheerio) {
      fullResponse = await cheerioDocRequest({
        uri: uri,
        qs: qs
      })
    } else {
      fullResponse = await rawDocRequest({
        uri: uri,
        qs: qs
      })
    }
  } else if (type === 'XHR') {
    fullResponse = await rawXHRRequest({
      uri: uri,
      qs: qs
    })
  }

  // Sleep for 1 sec between each requests to avoid being blocked by the vendor
  // await sleep(500)

  const statusCode = fullResponse.statusCode

  if (statusCode !== 200) {
    throw new Error(errors.VENDOR_DOWN)
  }

  return fullResponse
}

/*
A wrapper around request-promise requests to perform a POST request, checking the status code of the request.

Args:
  - uri: The URL of the request to perform
  - data: The data to POST
  - type: The type of request to perform. Can be "Doc" or "XHR"
  - cheerio: Indicates if cheerio should be used to parse the response

Returns:
- The full request
*/
async function POSTRequest(uri, data, type = 'Doc', cheerio = true) {
  // log('debug', `POST request to : ${uri}`)
  // log('debug', 'with data : ')
  // log('debug', data)
  // log('debug', 'of type : ' + type)
  // log('debug', 'with cheerio : ' + cheerio)

  let fullResponse

  if (type === 'Doc') {
    if (cheerio) {
      fullResponse = await cheerioDocRequest({
        uri: uri,
        method: 'POST',
        form: data
      })
    } else {
      fullResponse = await rawDocRequest({
        uri: uri,
        method: 'POST',
        form: data
      })
    }
  } else if (type === 'XHR') {
    fullResponse = await rawXHRRequest({
      uri: uri,
      method: 'POST',
      body: data
    })
  }

  // Sleep for 1 sec between each requests to avoid being blocked by the vendor
  // await sleep(500)

  const statusCode = fullResponse.statusCode

  if (statusCode !== 200) {
    log('debug', statusCode, 'error')
    throw new Error(errors.VENDOR_DOWN)
  }

  return fullResponse
}

module.exports = new BaseKonnector(start)

async function start(fields, cozyParameters) {
  if (cozyParameters) log('debug', 'Found COZY_PARAMETERS')

  // log('debug', rootPath)

  callRequestFactories()

  // Make GET request on HP
  // eslint-disable-next-line no-unused-vars
  const homePage = await GETRequest(BASE_URL)
  // log('debug', homePage.body.html())

  // Get the extra vars js to retrieve the API URL
  const extraVarsJS = await GETRequest(EXTRA_VARS_URL)
  // log('debug', extraVarsJS.body.html())

  const extraVarsJSContent = extraVarsJS.body.html()
  // log('debug', extraVarsJSContent)

  const backendUrlJson = extraVarsJSContent.match(
    /backend: {\n *url: '(.*)',\n *version: '(.*)',\n *}/
  )
  // log('debug', backendUrlJson)

  const baseBackendUrl = backendUrlJson[1]
  const backendVersion = backendUrlJson[2]

  BACKEND_URL = `${baseBackendUrl}${backendVersion}/`
  // log('debug', BACKEND_URL)

  // Authentification
  await authenticate.bind(this)(fields.login, fields.password)

  // Fetch available customer references
  const entities = await parseEntities(fields)
  // log('debug', entities)

  // Fetch and save invoices for each entity
  for (const entity of entities) {
    const invoicesList = await getInvoicesList(fields, entity)
    // log('debug', invoicesList)

    if (invoicesList.length) {
      await saveInvoices.bind(this)(fields, entity, invoicesList)
    }
  }

  await disconnect()
}

/** ********************************************************************************************************************/

async function authenticate(username, password) {
  log('info', 'Authenticating ...')

  const loginUrl = `${BACKEND_URL}login`

  const credentials = {
    username: username,
    password: password
  }

  const authenticateResponse = await POSTRequest(
    loginUrl,
    credentials,
    'XHR',
    false
  )
  // log('debug', authenticateResponse.body)

  authenticateToken = authenticateResponse.body.token
  // log('debug', authenticateToken)

  // Update request factories
  callRequestFactories()

  // Make GET request on authenticated URL to check that we are logged in
  const menuItemsUrl = `${BACKEND_URL}menus/menu_main/items`

  try {
    // eslint-disable-next-line no-unused-vars
    const menuItemsResponse = await GETRequest(menuItemsUrl, null, 'XHR', false)
    // log('debug', menuItemsResponse)
  } catch (err) {
    if (err.statusCode === 401) {
      throw new Error(errors.LOGIN_FAILED)
    } else {
      throw new Error(errors.VENDOR_DOWN)
    }
  }

  log('info', 'Successfully logged in')
}

/** ********************************************************************************************************************/

async function parseEntities(fields) {
  log('info', 'Fetching available customer references')

  const entitiesUrl = `${BACKEND_URL}web-methods/enseignes`

  const entitiesResponse = await GETRequest(entitiesUrl, null, 'XHR', false)
  // log('debug', entitiesResponse.body)

  let entities = entitiesResponse.body.enseignes
  entities = entities.map(entity => entity.enseigne)
  // log('debug', entities)

  if (fields['contractNumber']) {
    const entityId = fields.contractNumber.toString()

    entities = entities.filter(entity => entity.id === entityId)

    if (!entities.length) {
      throw new Error('Contract number is invalid for this account')
    }
  }

  log(
    'info',
    `Parse entities: The konnector will be run on ${entities.length} contract numbers`
  )

  return entities
}

/** ********************************************************************************************************************/
// Get list of all invoices to download
async function getInvoicesList(fields, entity) {
  log(
    'info',
    `Get invoices list for entity ${entity.id} (${entity.raisonSocial1})`
  )

  const invoicesUrl = `${BACKEND_URL}web-methods/factures`

  // Compute dates
  let fromDate = new Date('1900-1-1') // Start as much in the past as possible, except if onlyLastNMonths is set
  const toDate = new Date() // Until today

  if (fields.onlyLastNMonths && parseInt(fields.onlyLastNMonths) > 0) {
    fromDate = new Date()
    fromDate.setMonth(fromDate.getMonth() - parseInt(fields.onlyLastNMonths))
  }

  const dateDebutPeriode = `${fromDate.getFullYear()}-${fromDate.getMonth() +
    1}-${fromDate.getDate()}`
  const dateFinPeriode = `${toDate.getFullYear()}-${toDate.getMonth() +
    1}-${toDate.getDate()}`

  // Compute API params
  const invoicesParams = {
    dateDebutPeriode: dateDebutPeriode,
    dateFinPeriode: dateFinPeriode,
    enseigne: [entity.id]
  }
  // log('debug', invoicesParams)

  const invoicesResponse = await POSTRequest(
    invoicesUrl,
    invoicesParams,
    'XHR',
    false
  )
  // log('debug', invoicesResponse.body)

  let invoicesList = invoicesResponse.body.factures
  invoicesList = invoicesList.map(invoice => invoice.facture)
  // log('debug', invoicesList)

  log(
    'info',
    `Get invoices list: found ${invoicesList.length} invoices to download`
  )

  return invoicesList
}

/** ********************************************************************************************************************/
async function saveInvoices(fields, entity, invoicesList) {
  log('info', `Save invoices for entity ${entity.id} (${entity.raisonSocial1})`)

  const downloadUrl = `${BACKEND_URL}web-methods/download/facture/`
  const vendorSlug = slugify(VENDOR)
  const entitySlug = slugify(entity.raisonSocial1)
  // log('debug', entitySlug)

  for (const invoice of invoicesList) {
    const amountInclTax = parseFloat(invoice.montantTTC.replace(' ', ''))
    // log('debug', amountInclTax)

    const filename = `${vendorSlug}_${entity.id}_${entitySlug}_${invoice.numero}_${invoice.date}_${amountInclTax}EUR.pdf`
    // log('debug', filename)

    const fileURL = `${downloadUrl}${invoice.numero}`

    /* Unfortunately, we need to download the file twice:
      - Once with checkInvoiceURLAndContent function to check that the file is available (no empty response, 404, or 401 if the token has expired). In this last case, we need to refresh the token that has expired, and retry.
      During this first call, we don't download the file, we just check that it is available. If an error occurs with an error code, not in the list above, we throw an error to be aware that something went wrong on the vendor side.
      This is not ideal, but it is the only way to check that the file is available, because the saveBills function doesn't throw an error if something goes wrong during the download (https://github.com/konnectors/libs/blob/787e6a965d2b4393ec8244b0fb824d644119ed8e/packages/cozy-konnector-libs/src/libs/saveFiles.js#L241)

      - If the file exists and is not empty, we download the file with a promise calling saveBills and fetchInvoice.
    */

    let invoiceContent = undefined

    try {
      invoiceContent = await checkInvoiceURLAndContent(fileURL)
    } catch (err) {
      if (err.statusCode == 401) {
        // In this case, we need to refresh the token that has expired, and retry
        log('debug', 'Token expired, refreshing...')
        await authenticate.bind(this)(fields.login, fields.password)
        invoiceContent = await checkInvoiceURLAndContent(fileURL)
      } else if (err.statusCode != 404) {
        // If the status code is 404, do nothing otherwise throw an error
        throw err
      } else {
        log('debug', 'File is 404, skipping')
        continue
      }
    }

    if (!invoiceContent) {
      log('debug', 'File not available, skipping')
      continue
    }

    let document = [
      {
        filename: filename,
        amount: amountInclTax,
        date: parse(invoice.date, 'yyyy-MM-dd', new Date()),
        vendor: VENDOR,
        contractId: entity.id,
        title: invoice.numero,
        currency: '€',
        fileurl: fileURL,
        fetchFile: fetchInvoice // Custom function to download the file, because in some case, the server returns a 404 error, but the library doesn't handle it and creates a corrupted file
      }
    ]

    await this.saveBills(document, fields, {
      fileIdAttributes: ['filename'],
      contentType: 'application/pdf',
      identifiers: ['date', 'amount', 'vendor'],
      timeout: Date.now() + 15 * 1000
    })
    log('debug', 'File downloaded')
  }
}

async function checkInvoiceURLAndContent(fileURL) {
  // Try to get the invoice to validate that it is not empty, 404, and that token has not expired.
  // In this last case, we need to refresh the token that has expired (from above), and retry
  const fileContent = await GETRequest(fileURL, null, 'Doc', false)
  return fileContent?.body
}

async function fetchInvoice(document) {
  const fileURL = document.fileurl
  log('debug', `Downloading file ${fileURL}`)
  // Download the file with a promise that in practice works properly
  return rawDocRequest({
    uri: fileURL
  }).pipe(new stream.PassThrough())
}

async function disconnect() {
  log('info', 'Disconnecting from the provider website...')

  try {
    // eslint-disable-next-line no-unused-vars
    const logoutPage = await GETRequest(
      'https://connexiontotalpro.b2clogin.com/connexiontotalpro.onmicrosoft.com/oauth2/v2.0/logout',
      {
        p: 'b2c_1_clients-pro',
        post_logout_redirect_uri: 'https://pro.totalenergies.fr'
      },
      'Doc',
      false
    )
    // log('info', logoutPage.body)

    // eslint-disable-next-line no-unused-vars
    const logout2Page = await GETRequest(
      'https://connexiontotalpro.b2clogin.com/connexiontotalpro.onmicrosoft.com/v2.0/.well-known/openid-configuration',
      {
        p: 'B2C_1_clients-pro'
      },
      'XHR',
      false
    )
    // log('info', logout2Page.body)

    log('info', '✓ Disconnected')
  } catch (err) {
    log('error', err.message)
    throw new Error(errors.UNKNOWN_ERROR)
  }
}
